package es.jdvf.gestorhogar.gasto.util;

import es.jdvf.gestorhogar.gasto.dto.Gasto;
import es.jdvf.gestorhogar.gasto.entity.GastoEntity;

public class GastoUtil {

	/**
	 * Devuelve objeto DTO a partir de la entidad
	 * @param entity
	 * @return
	 */
	public static Gasto convertToDTO(GastoEntity entity) {
		return Gasto.builder().id(entity.getId())
				.tipo(entity.getTipo())
				.build();
	}
	
	/**
	 * Devuelve objeto entidad a partir del DTO
	 * @param dto
	 * @return
	 */
	public static GastoEntity convertToEntity(Gasto dto) {
		return GastoEntity.builder().id(dto.getId())
				.tipo(dto.getTipo())
				.build();
	}
}
