package es.jdvf.gestorhogar.gasto.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import es.jdvf.gestorhogar.gasto.entity.GastoEntity;

public interface GastoRepository extends MongoRepository<GastoEntity, String> {

}
