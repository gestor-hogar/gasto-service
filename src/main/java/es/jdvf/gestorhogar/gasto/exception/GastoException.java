package es.jdvf.gestorhogar.gasto.exception;

public class GastoException extends RuntimeException {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public GastoException(String id) {
        super(String.format("Exception with id: <%s>", id));
    }
}
