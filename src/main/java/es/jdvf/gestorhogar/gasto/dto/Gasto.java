package es.jdvf.gestorhogar.gasto.dto;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.Id;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Gasto {
	// Identificador
	@Id
	private String id;
	// tipo
	@NotEmpty
	private String tipo;
	// descripcion
	private String descripcion;
	// cantidad
	private double cantidad;
	// fecha
	private String fecha;
}
