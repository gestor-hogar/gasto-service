package es.jdvf.gestorhogar.gasto.entity;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
@Document(collection = "gasto")
public class GastoEntity {
	// id
	private String id;
	// tipo
	private String tipo;
	// descripcion
	private String descripcion;
	// cantidad
	private double cantidad;
	// fecha
	private String fecha;
}