package es.jdvf.gestorhogar.gasto.service;

import java.util.List;

import es.jdvf.gestorhogar.gasto.dto.Gasto;

public interface GastoService {

	List<Gasto> avisos(String email);

}
