package es.jdvf.gestorhogar.gasto.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import es.jdvf.gestorhogar.gasto.dto.Gasto;
import es.jdvf.gestorhogar.gasto.exception.GastoException;
import es.jdvf.gestorhogar.gasto.service.GastoService;
import io.swagger.annotations.ApiOperation;

/**
 * Controller gestión gastos
 *
 */
@RestController
@RequestMapping("/api/gasto")
public class GastoController {
	private static final Logger LOGGER = LoggerFactory.getLogger(GastoController.class);

	@Autowired
	private GastoService service;

	/**
	 * Devuelve avisos del usuario de cambios en los gastos
	 * 
	 * @param email
	 * @return
	 */
	@ApiOperation(value = "Devuelve avisos del usuario de cambios en los gastos.", response = Gasto.class)
	@RequestMapping(value = "/{email}/avisos", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.CREATED)
	ResponseEntity<List<Gasto>> avisos(@PathVariable("email") String email) {
		LOGGER.info("avisos - begin - email: {}", email);
		List<Gasto> result = service.avisos(email);
		LOGGER.info("avisos - result: {}", result);
		return ResponseEntity.ok(result);
	}

	@ExceptionHandler
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public void handleTodoNotFound(GastoException ex) {
		LOGGER.error("Handling error with message: {}", ex.getMessage());
	}
}
